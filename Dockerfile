FROM node:16.13-alpine as build
ENV __BASE_HREF__=/ui
WORKDIR /usr/src/app

COPY package.json package-lock.json ./
RUN npm install

COPY . .
RUN npm run build -- --base-href=/ui/ --deploy-url=/ui/

FROM nginx:1.21.5-alpine

COPY .nginx/nginx.conf /etc/nginx/nginx.conf
COPY --from=build /usr/src/app/dist/ory-hydra-admin-ui /usr/share/nginx/html
