export const environment = {
  production: true,
  hydra_url: 'https://ory-hydra-admin.dewarrum.me/api'
};
