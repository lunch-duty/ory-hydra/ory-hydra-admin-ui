import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {ClientModel} from "../models/client.model";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class HydraService {

  private readonly uri: string;

  constructor(private httpClient: HttpClient) {
    this.uri = environment.hydra_url;
  }

  public list(limit: number, offset: number, client_name?: string, owner?: string): Observable<ClientModel[]> {
    let httpParams = new HttpParams()
      .set('limit', limit)
      .set('offset', offset);

    if (client_name)
      httpParams = httpParams.set('client_name', client_name);

    if (owner)
      httpParams = httpParams.set('owner', owner);

    return this.httpClient.get<ClientModel[]>(`${this.uri}/clients`, {
      params: httpParams
    });
  }
}
