import { TestBed } from '@angular/core/testing';

import { HydraService } from './hydra.service';

describe('HydraService', () => {
  let service: HydraService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HydraService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
