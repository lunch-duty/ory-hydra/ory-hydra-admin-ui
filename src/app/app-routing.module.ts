import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'clients', loadChildren: () => import('./hydra/clients/clients.module').then(m => m.ClientsModule)},
  { path: 'consent-requests', loadChildren: () => import('./hydra/consent-requests/consent-requests.module').then(m => m.ConsentRequestsModule) },
  { path: '**', redirectTo: 'clients'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
