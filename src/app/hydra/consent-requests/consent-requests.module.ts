import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConsentRequestsRoutingModule } from './consent-requests-routing.module';
import { ListComponent } from './components/list/list.component';


@NgModule({
  declarations: [
    ListComponent,
  ],
  imports: [
    CommonModule,
    ConsentRequestsRoutingModule,
  ]
})
export class ConsentRequestsModule { }
