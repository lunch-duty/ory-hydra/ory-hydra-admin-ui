import { Component, OnInit } from '@angular/core';
import {HydraService} from "../../../../services/hydra.service";
import {Observable} from "rxjs";
import {ClientModel} from "../../../../models/client.model";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public readonly clients$: Observable<ClientModel[]>;

  constructor(private hydraService: HydraService) {
    this.clients$ = hydraService.list(20, 0);
  }

  ngOnInit(): void {
  }

}
