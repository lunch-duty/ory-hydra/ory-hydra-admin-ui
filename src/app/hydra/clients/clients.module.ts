import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './components/list/list.component';
import {ClientsRoutingModule} from "./clients-routing.module";
import {HttpClientModule} from "@angular/common/http";
import {MatCardModule} from "@angular/material/card";



@NgModule({
  declarations: [
    ListComponent
  ],
    imports: [
        CommonModule,
        ClientsRoutingModule,
        HttpClientModule,
        MatCardModule,
    ]
})
export class ClientsModule { }
